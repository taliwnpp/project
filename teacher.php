<?php include 'navbar.php';?>
<?php include 'head.php';?>
<?php include 'config.php';?>
<?php
$query="select * from db_framework.teacher";
$result=mysqli_query($con,$query);
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<style>
   #output{
    width: 200px;
    height: 200px;
    
   }

   .card-header{
  background-color: #ABBEEC;
}
</style>
<body>

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

     

        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>ข้อมูลส่วนตัว</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li><a href="#">Forms</a></li>
                                    <li class="active">Basic</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="animated fadeIn">


                <div class="row">
                    

                  

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong>รายชื่อบุคลากร</strong>
                            </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="5%"></th>
                                    <th width="20%">ชื่อ</th>
                                    <th width="20%">นามสกุล</th>
                                    <th width="20%">อีเมล</th>
                                    <th width="20%">เบอร์โทร</th>
                                    <th width="20%">เพิ่มเติม</th>
                                </tr>
                                <?php while($row=mysqli_fetch_array($result)){?>
                                <tr>
                                    <td><?php echo $row['id']?></td>
                                    <td><?php echo $row['teacher_name']?></td>
                                    <td><?php echo $row['teacher_lname']?></td>
                                    <td><?php echo $row['teacher_email']?></td>
                                    <td><?php echo $row['teacher_tel']?></td>
                                    <td><input type="button" name="view" value="เพิ่มเติม" class="view_tea"></td>
                                </tr>
                               <?php }; ?>
                            </table>
                        </div>




                          
                        </div>
                       
                    </div>


                  

            

         

        </div><!-- .animated -->
      
    </div><!-- .content -->




    <div class="clearfix"></div>

    <?php include 'footer.php';?>
    <?php require 'modal-teacher.php' ?>       


</div><!-- /#right-panel -->

<!-- Right Panel -->

<!-- Scripts -->
<script>
 $(document).ready(function(){
    $('.view_tea').click(function(){
        $('#dataModal').modal('show');
    });
 });
    
</script>


</body>
</html>
