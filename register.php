<?php 
include 'navbar.php';
include 'head.php';
include 'config.php';

$sql="select * from db_framework.provinces";
$result=mysqli_query($con,$sql);

?>


<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<style>
   #output{
    width: 200px;
    height: 200px;
    
   }

   .card-header{
  background-color: #fadee1;
}
</style>
<body>

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

     

        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>ข้อมูลส่วนตัว</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li><a href="#">Forms</a></li>
                                    <li class="active">Basic</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="animated fadeIn">


                <div class="row">
                    

                  

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong>ข้อมูลนักเรียน</strong>
                            </div>
                            <div class="card-body card-block">
                                <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label class=" form-control-label">รูปภาพ :</label></div>
                                        <input  type="file" accept="image/*" onchange="loadFile(event)"><img id="output"/>
                                    </div>



                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">เลขประจำตัวประชาชน :</label></div>
                                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="email-input" class=" form-control-label">เลขประจำตัวนักเรียน :</label></div>
                                        <div class="col-12 col-md-9"><input type="email" id="email-input" name="email-input" placeholder="Enter Email" class="form-control"><small class="help-block form-text">Please enter your email</small></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="password-input" class=" form-control-label">ชื่อ-สกุล :</label></div>
                                        <div class="col-12 col-md-9"><input type="password" id="password-input" name="password-input" placeholder="Password" class="form-control"><small class="help-block form-text">Please enter a complex password</small></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">ชื่อ-สกุลภาษาอังกฤษ :</label></div>
                                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">วัน/เดือน/ปีเกิด :</label></div>
                                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">เชื้อชาติ :</label></div>
                                        <div class="col-12 col-md-3"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">สัญชาติ :</label></div>
                                        <div class="col-12 col-md-3"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">ศาสนา :</label></div>
                                        
                                        <div class="col-12 col-md-3">
                                        <select name="select" id="select" class="form-control">
                                                 <option value="0"></option>
                                                <option value="1">พุทธ</option>
                                                <option value="2">อิสลาม</option>
                                                <option value="3">คริสต์</option>
                                                <option value="4">พราหมณ์-ฮินดู</option>
                                                <option value="5">ซิกข์</option>
                                            
                                            </select>
                                        </div>
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">ชนเผ่า :</label></div>
                                        <div class="col-12 col-md-3"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">ชื่อผู้ปกครอง :</label></div>
                                        <div class="col-12 col-md-3"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">นามสกุล :</label></div>
                                        <div class="col-12 col-md-3"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">เบอร์โทรศัพท์ :</label></div>
                                        <div class="col-12 col-md-3"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">อีเมล :</label></div>
                                        <div class="col-12 col-md-3"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">จำนวนพี่น้อง :</label></div>
                                        <div class="col-12 col-md-3"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                                    </div>
                                    
                                   
                                    
                                </form>
                            </div>

                          
                        </div>
                       
                    </div>

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong>ที่อยู่ปัจจุบัน</strong>
                            </div>
                            <div class="card-body card-block">

                            <div class="row form-group">
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">รหัสทะเบียนบ้าน :</label></div>
                                        <div class="col-12 col-md-3"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">เลขที่บ้าน :</label></div>
                                        <div class="col-12 col-md-3"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                            </div>
                             <div class="row form-group">
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">หมู่ที่ :</label></div>
                                        <div class="col-12 col-md-3"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                             </div>
                             <div class="row form-group">
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">ถนน :</label></div>
                                        <div class="col-12 col-md-3"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                             </div>
                             <div class="row form-group">
                                        
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">จังหวัด :</label></div>
            
                                        <div class="col-12 col-md-3">
                                        <select name="provinces" id="provinces" class="form-control">
                                            <option value="" selected disabled>กรุณาเลือกจังหวัด</option>
                                                <?php foreach ($result as $value) { ?>
                                                 <option value="<?=$value['id']?>"><?=$value['name_th']?></option>
                                                 <?php } ?>
                                            
                                        </select>
                                        </div>


                                       <div class="col col-md-3"><label for="text-input" class=" form-control-label">อำเภอ :</label></div>
                                        
                                       <div class="col-12 col-md-3">
                                        <select name="amphures" id="amphures" class="form-control">
                                       
                                               
                                            
                                        </select>
                                        </div>
                            </div>
                            <div class="row form-group">
                                         <div class="col col-md-3"><label for="text-input" class=" form-control-label">ตำบล/แขวง :</label></div>
                                            
                                       <div class="col-12 col-md-3">
                                        <select name="districts" id="districts" class="form-control">
                                       
                                        </select>
                                        </div>

                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">รหัสไปรษณีย์ :</label></div>
                                            
                                            <div class="col-12 col-md-3">
                                                
                                             <input type="text" name="zipcode" id="zipcode" class="form-control">
                                             
                                            
                                             </div>
                                        
                                        
                            </div>
                            </div>
                            </div>

            </div>


            

            <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong>ที่อยู่ตามทะเบียนบ้าน</strong>
                            </div>
                            <div class="card-body card-block">

                            <div class="row form-group">
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">รหัสทะเบียนบ้าน :</label></div>
                                        <div class="col-12 col-md-3"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">เลขที่บ้าน :</label></div>
                                        <div class="col-12 col-md-3"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                            </div>
                             <div class="row form-group">
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">หมู่ที่ :</label></div>
                                        <div class="col-12 col-md-3"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                             </div>
                             <div class="row form-group">
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">ถนน :</label></div>
                                        <div class="col-12 col-md-3"><input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control"><small class="form-text text-muted">This is a help text</small></div>
                             </div>
                             <div class="row form-group">
                                        
                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">จังหวัด :</label></div>
            
                                        <div class="col-12 col-md-3">
                                        <select name="h_provinces" id="h_provinces" class="form-control">
                                            <option value="" selected disabled>กรุณาเลือกจังหวัด</option>
                                                <?php foreach ($result as $value) { ?>
                                                 <option value="<?=$value['id']?>"><?=$value['name_th']?></option>
                                                 <?php } ?>
                                            
                                        </select>
                                        </div>


                                       <div class="col col-md-3"><label for="text-input" class=" form-control-label">อำเภอ :</label></div>
                                        
                                       <div class="col-12 col-md-3">
                                        <select name="h_amphures" id="h_amphures" class="form-control">
                                       
                                               
                                            
                                        </select>
                                        </div>
                            </div>
                            <div class="row form-group">
                                         <div class="col col-md-3"><label for="text-input" class=" form-control-label">ตำบล/แขวง :</label></div>
                                            
                                       <div class="col-12 col-md-3">
                                        <select name="h_districts" id="h_districts" class="form-control">
                                       
                                        </select>
                                        </div>

                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">รหัสไปรษณีย์ :</label></div>
                                            
                                            <div class="col-12 col-md-3">
                                                
                                             <input type="text" name="h_zipcode" id="h_zipcode" class="form-control">
                                             
                                            
                                             </div>
                                        
                                        
                            </div>
                            </div>
                            </div>

            </div>


        </div><!-- .animated -->
        <div align="right">
        <button type="button" class="btn btn-info" >บันทึกข้อมูล</button>
        <button type="button" class="btn btn-danger" >ปิด</button>
        </div>
    </div><!-- .content -->




    <div class="clearfix"></div>

    <?php include 'footer.php';?>

</div><!-- /#right-panel -->

<!-- Right Panel -->

<!-- Scripts -->
<script>
  var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output');
      output.src = reader.result;
   

    };
    reader.readAsDataURL(event.target.files[0]);
  };

$('#provinces').change(function(){
    var id_provinces = $(this).val();
    /*console.log($(this).val());  ไว้เช็คค่า*/
    $.ajax({
        type : "post", /*ประเภทการส่ง */
        url : "ajax_current.php", /* ส่งข้อมูลไปสู่ */
        data : {id:id_provinces,function :'provinces'}, /* เก็บข้อมูลไปด้วย */ 
        success: function(data){ /*  เมื่อส่งค่าไปได้ ส่งกลับมาคือ data*/
          $('#amphures').html(data);

          
        }
    });
});


$('#amphures').change(function(){
    var id_amphures = $(this).val();
    /*console.log($(this).val());  ไว้เช็คค่า*/
    $.ajax({
        type : "post", /*ประเภทการส่ง */
        url : "ajax_current.php", /* ส่งข้อมูลไปสู่ */
        data : {id:id_amphures,function :'amphures'}, /* เก็บข้อมูลไปด้วย */ 
        success: function(data){ /*  เมื่อส่งค่าไปได้ ส่งกลับมาคือ data*/
           /* console.log(data)*/
          $('#districts').html(data);

          
        }
    });
});



$('#districts').change(function(){
    var id_districts = $(this).val();
    /*console.log($(this).val());  ไว้เช็คค่า*/
    $.ajax({
        type : "post", /*ประเภทการส่ง */
        url : "ajax_current.php", /* ส่งข้อมูลไปสู่ */
        data : {id:id_districts,function :'districts'}, /* เก็บข้อมูลไปด้วย */ 
        success: function(data){ /*  เมื่อส่งค่าไปได้ ส่งกลับมาคือ data*/
          $('#zipcode').val(data)

          
        }
    });
});

/* --------------------------------------------------------------------------------------------------------- */
$('#h_provinces').change(function(){
    var id_provinces = $(this).val();
    /*console.log($(this).val());  ไว้เช็คค่า*/
    $.ajax({
        type : "post", /*ประเภทการส่ง */
        url : "ajax_house_registration.php", /* ส่งข้อมูลไปสู่ */
        data : {id:id_provinces,function :'provinces'}, /* เก็บข้อมูลไปด้วย */ 
        success: function(data){ /*  เมื่อส่งค่าไปได้ ส่งกลับมาคือ data*/
          $('#h_amphures').html(data);

          
        }
    });
});


$('#h_amphures').change(function(){
    var id_amphures = $(this).val();
    /*console.log($(this).val());  ไว้เช็คค่า*/
    $.ajax({
        type : "post", /*ประเภทการส่ง */
        url : "ajax_house_registration.php", /* ส่งข้อมูลไปสู่ */
        data : {id:id_amphures,function :'amphures'}, /* เก็บข้อมูลไปด้วย */ 
        success: function(data){ /*  เมื่อส่งค่าไปได้ ส่งกลับมาคือ data*/
           /* console.log(data)*/
          $('#h_districts').html(data);

          
        }
    });
});



$('#h_districts').change(function(){
    var id_districts = $(this).val();
    /*console.log($(this).val());  ไว้เช็คค่า*/
    $.ajax({
        type : "post", /*ประเภทการส่ง */
        url : "ajax_house_registration.php", /* ส่งข้อมูลไปสู่ */
        data : {id:id_districts,function :'districts'}, /* เก็บข้อมูลไปด้วย */ 
        success: function(data){ /*  เมื่อส่งค่าไปได้ ส่งกลับมาคือ data*/
          $('#h_zipcode').val(data)

          
        }
    });
});




</script>


</body>
</html>
