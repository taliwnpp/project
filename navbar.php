 <!-- Left Panel -->

 <style>
    .image-user{
        width: 150px;
        height: 150px;
        border-radius: 50%;
        margin: 20px;
        object-fit: cover;
        object-position: center ;

    }
    .wrapper{
        text-align: center;
        background-color: #f6baba;
    }
 </style>
 <aside id="left-panel" class="left-panel">
 <div class="wrapper">
                 <img  class="image-user"src="images/S__20881416.jpg"><br>
                 <strong >ชื่อคนเข้าระบบ</strong><br>
                 <strong >ตำแหน่ง</strong>
                </div>
                
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
              
                    <li class="">
                        <a href="#"><i class="menu-icon fa fa-laptop"></i>หน้าแรก</a>
                       
                    <li class="menu-title">บุคลากรและนักเรียน</li><!-- /.menu-title -->
                   
                    <li>
                    <a href="register.php"><i class="menu-icon fa fa-laptop"></i>ลงทะเบียนนักเรียน </a>
                        <a href="register_order.php"><i class="menu-icon fa fa-laptop"></i>รายชื่อผู้ลงทะเบียน </a>
                        <a href="#"><i class="menu-icon fa fa-laptop"></i>ข้อมูลนักเรียน </a>
                        <a href="teacher.php"><i class="menu-icon fa fa-laptop"></i>ข้อมูลบุคลากร </a>
                        <a href="user.php"><i class="menu-icon fa fa-laptop"></i>ผู้ดูแลระบบ </a>
                    </li>
                    </li>




                    <li class="menu-title">ประเมินนักเรียน</li><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cogs"></i>นักเรียนที่ดูแล</a>
                        <ul class="sub-menu children dropdown-menu"><li><i class="fa fa-puzzle-piece"></i><a href="ui-buttons.html">Buttons</a></li>
                            <li><i class="fa fa-id-badge"></i><a href="ui-badges.html">Badges</a></li>
                            <li><i class="fa fa-bars"></i><a href="ui-tabs.html">Tabs</a></li>

                            <li><i class="fa fa-id-card-o"></i><a href="ui-cards.html">Cards</a></li>
                            <li><i class="fa fa-exclamation-triangle"></i><a href="ui-alerts.html">Alerts</a></li>
                            <li><i class="fa fa-spinner"></i><a href="ui-progressbar.html">Progress Bars</a></li>
                            <li><i class="fa fa-fire"></i><a href="ui-modals.html">Modals</a></li>
                            <li><i class="fa fa-book"></i><a href="ui-switches.html">Switches</a></li>
                            <li><i class="fa fa-th"></i><a href="ui-grids.html">Grids</a></li>
                            <li><i class="fa fa-file-word-o"></i><a href="ui-typgraphy.html">Typography</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>ประเมิน</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="tables-basic.html">Basic Table</a></li>
                            <li><i class="fa fa-table"></i><a href="tables-data.html">Data Table</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>ข้อมูลการประเมิน</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-th"></i><a href="forms-basic.html">Basic Form</a></li>
                            <li><i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Advanced Form</a></li>
                        </ul>
                    </li>

                    <li class="menu-title">ตั้งค่า</li><!-- /.menu-title -->

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>ข้อมูลแบบประเมิน</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-fort-awesome"></i><a href="font-fontawesome.html">หัวข้อแบบประเมิน</a></li>
                            <li><i class="menu-icon ti-themify-logo"></i><a href="font-themify.html">ประเภทพัฒนาการ</a></li>
                            <li><i class="menu-icon ti-themify-logo"></i><a href="font-themify.html">ข้อมูลคำถาม</a></li>
                            
                        </ul>
                    </li>
                    <li>
                        <a href="widgets.html"> <i class="menu-icon ti-email"></i>ออกจากระบบ </a>
                    </li>
                   
                    
                  
                   
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
    <!-- /#left-panel -->