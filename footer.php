<footer class="site-footer">
        <div class="footer-inner bg-white">
            <div class="row">
                <div class="col-sm-6">
                    Copyright &copy; 2022 ศูนย์การศึกษาพิเศษ | เขตการศึกษาที่ 1 นครปฐม
                </div>
                <div class="col-sm-6 text-right">
                    Designed by <a href="https://colorlib.com">นฤมล</a>
                </div>
            </div>
        </div>
    </footer>